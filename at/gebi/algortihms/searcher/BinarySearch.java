package at.gebi.algortihms.searcher;

import at.gebi.algortihms.helper.DataGenerator;

public class BinarySearch {
	  public static int search(int searchFor) throws NumberNotFoundException {
	    int result = 0;
	    int[] array = DataGenerator.generateRandomData(30, 10, 100);
	    
	    for (int i = 0; i < array.length; i++) {
	      if(searchFor == array[i]) {
	        System.out.println("Die Zahl " + searchFor + " findest du auf Index " + i);
	      }
	      else if(i == array.length) {
	        throw new NumberNotFoundException("Zahl nicht gefunden");
	      }
	    }    
	    return result;
	  }
	}