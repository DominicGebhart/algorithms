package at.gebi.algortihms.searcher;

public class Rekursion {
	public static void main(String [ ] args)
	{
		System.out.println(getGGT(25,100));
	}
	private static int getGGT(int a, int b)
	{
		System.out.println(a + ":" + b);
		if(a==b){
			return a;
		}else if(a>b){
			return getGGT(a-b,b);
		}else{
			return getGGT(a,b-a);
		}
	}
	
	private static int addRec(int a, int count) {
		if (count == 3) {
			return a;
		} else {
			return a + addRec(a, ++count);
		}
	}
	
	private static int getFactorial(int num){
		System.out.println(num);
		if(num==1){
			return 1;
		}else{
			return num * getFactorial(++num);
		}
	}
	
	private static int potenz(int x, int p) {
		if (p == 1) {
			return x;
		} else {
			return x * potenz(x, --p);
		}
	}
	
	private static void printHex(int x) {
		if (x <= 16) {
			System.out.print(Integer.toHexString(x % 16));
		} else {
			printHex(x / 16);
			System.out.print(Integer.toHexString(x % 16));
		}
	}
	
	private static int fibonacci(int x) {
		if (x == 1 || x == 2) {
			return 1;
		} else {

			return fibonacci(x - 1) + fibonacci(x - 2);
		}
	}
	
	private static boolean isPalindrom(String str){
		if(str.length() == 0 || str.length() == 1)
            return true; 
        if(str.charAt(0) == str.charAt(str.length()-1))
            return isPalindrom(str.substring(1, str.length()-1));
        else{
        	return false;
        }

	}
}
