package at.gebi.algortihms.searcher;

public class NumberNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	NumberNotFoundException(String reason)
	{
		super(reason);
	}
}
