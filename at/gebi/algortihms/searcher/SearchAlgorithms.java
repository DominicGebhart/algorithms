package at.gebi.algortihms.searcher;

public interface SearchAlgorithms {
	public int Search(int[] a, int s) throws NumberNotFoundException;
}
