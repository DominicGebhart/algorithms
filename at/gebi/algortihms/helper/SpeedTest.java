package at.gebi.algortihms.helper;

import java.util.ArrayList;

import at.gebi.algortihms.sorters.SortAlgorithm;

public class SpeedTest {

	private ArrayList<SortAlgorithm> algorithms;

	public SpeedTest() {
		this.algorithms = new ArrayList<SortAlgorithm>();
	}

	public void addAlgorithm(SortAlgorithm sa) {
		algorithms.add(sa);
	}

	public void run() {
		int[] toSort = DataGenerator.generateRandomData(500, 1, 50);
		for (SortAlgorithm sa : algorithms) {
			System.out.println(sa.getName());
			long startTime = System.nanoTime();
			sa.Sort(toSort);
			long endTime = System.nanoTime();
			long duration = (endTime - startTime) / 1000000;
			System.out.println(duration);
		}
	}

}
