package at.gebi.algortihms.helper;

public class Node<E>{
    E elem;
    Node<E> next, previous;
}