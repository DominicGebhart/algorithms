package at.gebi.algortihms.helper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class DataGenerator {
	public static int[] generateRandomData (int size, int minValue, int maxValue)
	{
		Random rand = new Random();
		//LinkedList<Integer> data = new LinkedList<Integer>();
		int[] randdata = new int[size];
		
		for(int i = 0; i<size; i++)
		{
			int f = rand.nextInt(maxValue+1);
			
			while(f<=minValue)
			{
				f = rand.nextInt(maxValue+1);
			}
		
			//data.add(f);
			randdata[i] = f;		
		}

		return randdata;
	}
	public static void printData(int[] data)
	{
		ArrayList<Integer> list = new ArrayList<>();
		for(int x : data)
		{
			list.add(x);
		}
		System.out.println(list.toString());
	}
}
