package at.gebi.algortihms.sorters;

import java.util.ArrayList;

public class SelectionSorter implements SortAlgorithm{

	@Override
	public int[] Sort(int[] data) {
		ArrayList<Integer> list = new ArrayList<>();
		for(int x : data)
		{
			list.add(x);
		}
		
		if(list.size()>1)
		{
			int size = list.size();
			for(int i = 0; i < list.size() - 1; i++)
			{
				int min = i;
				
				for(int element = i+1; element < size; element++)
				{
					if(list.get(element)<list.get(min))
					{
						min = element;
					}
				}
				
				if(min != i){
					int temp = list.get(min);
					list.set(min, list.get(i));
					list.set(i, temp);
				}
			}
		}
		
		//System.out.println(list.toString());
		int[] sorterList = new int[list.size()];
		for(int e = 0; e < list.size(); e++) sorterList[e] = list.get(e);
		return sorterList;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Selection Sorter";
	}

}
