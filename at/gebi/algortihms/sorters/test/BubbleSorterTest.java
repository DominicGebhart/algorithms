package at.gebi.algortihms.sorters.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.gebi.algortihms.sorters.BubbleSorter;

public class BubbleSorterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		int[] toSort = {15,0,46,23,17,32,88,61};
		int[] finished = {0,15,17,23,32,46,61,88};
		BubbleSorter ss = new BubbleSorter();
		int[] result = ss.Sort(toSort);
		assertArrayEquals(finished, result);
	}

}
