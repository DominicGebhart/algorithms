package at.gebi.algortihms.sorters.test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import at.gebi.algortihms.sorters.SelectionSorter;

public class SelectionSorterTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSort() {
		int[] toSort = {15,0,46,23,17,32,88,61};
		int[] finished = {0,15,17,23,32,46,61,88};
		SelectionSorter ss = new SelectionSorter();
		int[] result = ss.Sort(toSort);
		assertArrayEquals(finished, result);
	}

}
