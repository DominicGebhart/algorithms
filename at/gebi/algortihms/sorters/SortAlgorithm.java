package at.gebi.algortihms.sorters;

public interface SortAlgorithm {
	public int[] Sort (int[] data);
	public String getName();
}
