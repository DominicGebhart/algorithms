package at.gebi.algortihms.sorters;

import java.util.ArrayList;

public class BubbleSorter implements SortAlgorithm{

	@Override
	public int[] Sort(int[] data) {
		ArrayList<Integer> list = new ArrayList<>();
		for(int x : data)
		{
			list.add(x);
		}
		
		if(list.size()>1)
		{
			for(int x=0; x<list.size(); x++)
			{
				for(int i=0; i < list.size()- x - 1; i++)
				{
					if(list.get(i).compareTo(list.get(i+1)) > 0)
					{
						int tempVar = list.get(i);
						list.set(i, list.get(i+1));
						list.set(i+1, tempVar);
					}
				}
			}
		}
		
		//System.out.println(list.toString());
		int[] sorterList = new int[list.size()];
		for(int e = 0; e < list.size(); e++) sorterList[e] = list.get(e);
		return sorterList;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Bubble Sorter";
	}
}
