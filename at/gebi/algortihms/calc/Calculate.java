package at.gebi.algortihms.calc;

import java.util.ArrayList;

public class Calculate {
	public static int Addition(int Number1, int Number2)
	{
		ArrayList<Integer> Number1Digits = new ArrayList<>();
		ArrayList<Integer> Number2Digits = new ArrayList<>();
		while (Number1 > 0) {
		  int div = Number1 / 10;
		  int digit = Number1 - div * 10;
		  Number1 = div;
//		  System.out.println(digit);
		  Number1Digits.add(digit);
		}
//		System.out.println("\n");
		while (Number2 > 0) {
		  int div = Number2 / 10;
		  int digit = Number2 - div * 10;
		  Number2 = div;
//		  System.out.println(digit);
		  Number2Digits.add(digit);
		}
//		System.out.println("\n");
		ArrayList<Integer> Sum = new ArrayList<>();
		if(Number1Digits.size()>=Number2Digits.size())
		{		
			int temp = 0;
			for(int index = 0; index < Number1Digits.size(); index++){
				if(Number2Digits.size()-1<index){
					int x = Number1Digits.get(index) + Number2Digits.get(index);
					x = x+temp;
					temp = 0;
					if(x>9){
						 int div = x / 10;
						 int digit = x - div * 10;
						 Sum.add(digit);
						 temp = div;
					}else{
						Sum.add(x);
					}
				}else{
					Sum.add(Number1Digits.get(index));
				}
			}
			if(temp>0){
				Sum.add(temp);
			}
		}else{
			
		}
		String sresult = "";
		int result = 0;
		int d = Sum.size()-1;
		while(d>=0)
		{
			sresult = sresult + String.valueOf(Sum.get(d));
			d=d-1;
		}
		
		if(sresult!=""){
			result = Integer.parseInt(sresult);
		}
		System.out.println(result);
		return result;
	}
}
