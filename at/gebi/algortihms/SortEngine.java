package at.gebi.algortihms;

import at.gebi.algortihms.sorters.SortAlgorithm;

public class SortEngine {
	private SortAlgorithm sortAlgorithm;
	
	public int[] sort(int[] data){
		return sortAlgorithm.Sort(data);
	}
	
	public void setAlgorithm(SortAlgorithm algo){
		this.sortAlgorithm = algo;
	}
}
