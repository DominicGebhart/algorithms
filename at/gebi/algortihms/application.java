package at.gebi.algortihms;

import at.gebi.algortihms.calc.Calculate;
import at.gebi.algortihms.helper.DataGenerator;
import at.gebi.algortihms.helper.SpeedTest;
import at.gebi.algortihms.sorters.BubbleSorter;
import at.gebi.algortihms.sorters.SelectionSorter;

public class application {
	public static void main(String [ ] args)
	{
//	      int[] data = DataGenerator.generateRandomData(10, 1, 40);
//	      DataGenerator.printData(data);
//	      SortEngine se = new SortEngine();
//	      se.setAlgorithm(new SelectionSorter());
//	      se.sort(data);

		  SpeedTest st = new SpeedTest();
		  st.addAlgorithm(new BubbleSorter());
		  st.addAlgorithm(new SelectionSorter());
		  st.run();
	}
}
